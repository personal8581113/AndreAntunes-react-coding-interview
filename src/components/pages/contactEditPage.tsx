import { useContactEdit } from '@hooks/contacts/useContactEdit';
import { useUIContext } from '@hooks/useUIContext';
import { Group } from '@lib/models/person';
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  LinearProgress,
  TextField,
  Typography
} from '@mui/material';
import React from 'react';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

export default function ContactEditPage() {
  const { id } = useParams();
  const { contact, update, loading } = useContactEdit(id);
  const [email, setEmail] = useState(contact?.email);
  const [firstName, setFirstName] = useState(contact?.firstName);
  const [lastName, setLastName] = useState(contact?.lastName);
  const [groups, setGroups] = useState<Group[]>(contact?.groups || []);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const updatedContact = {
      id,
      firstName: firstName || contact?.firstName,
      lastName: lastName || contact?.lastName,
      email: email || contact?.email,
      groups: groups || []
    };
    update(updatedContact);
  };

  const { navbarInteractivePortal } = useUIContext();

  const onGroupCheck = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { checked, value } = e.target;
    if (checked) {
      setGroups([...groups, value as Group]);
    } else {
      setGroups(groups.filter(g => g !== value));
    }
  };

  useEffect(() => {
    if (contact?.groups) {
      setGroups(contact.groups);
    }
  }, [contact]);

  console.log('groups state --->', groups);

  if (!contact) {
    return (
      <Box>
        {navbarInteractivePortal(
          <Box
            height="100%"
            display="flex"
            alignItems="center"
            justifyContent="end"
            pl={8}>
            {loading && <LinearProgress sx={{ width: '100%' }} />}
            {!loading && <Typography variant="caption">Edit</Typography>}
          </Box>
        )}
      </Box>
    );
  }

  return (
    <Box p={4} overflow="auto">
      <form onSubmit={handleSubmit}>
        <Box mb={2}>
          <FormControl>
            <TextField
              label="E-mail"
              defaultValue={contact.email}
              onChange={e => setEmail(e.target.value)}
            />
          </FormControl>
        </Box>
        <Box mb={2}>
          <FormControl>
            <TextField
              label="First Name"
              defaultValue={contact.firstName}
              onChange={e => setFirstName(e.target.value)}
            />
          </FormControl>
        </Box>
        <Box mb={2}>
          <FormControl>
            <TextField
              label="Last Name"
              defaultValue={contact.lastName}
              onChange={e => setLastName(e.target.value)}
            />
          </FormControl>
        </Box>
        <Box mb={2}>
          <Typography>Groups</Typography>
          <FormControlLabel
            value="PRIMARY"
            control={
              <Checkbox
                defaultChecked={contact.groups?.includes('PRIMARY')}
                onChange={onGroupCheck}
              />
            }
            label="PRIMARY"
            labelPlacement="bottom"
          />
          <FormControlLabel
            value="SECONDARY"
            control={
              <Checkbox
                defaultChecked={contact.groups?.includes('SECONDARY')}
                onChange={onGroupCheck}
              />
            }
            label="SECONDARY"
            labelPlacement="bottom"
          />
        </Box>
        <Box>
          <Button variant="contained" type="submit" disabled={loading}>
            Save
          </Button>
        </Box>
      </form>
      {navbarInteractivePortal(
        <Box height="100%" display="flex" alignItems="center" justifyContent="end" pl={8}>
          {loading && <LinearProgress sx={{ width: '100%' }} />}
          {!loading && <Typography variant="caption">Edit</Typography>}
        </Box>
      )}
    </Box>
  );
}
