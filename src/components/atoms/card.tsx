import { Box, styled } from '@mui/material';

export const Card = styled(Box)(({ theme }) => ({
  position: 'relative',
  background: '#FFF',
  boxShadow: theme.shadows[2],
  padding: theme.spacing(3),
  borderRadius: 8,
  cursor: 'pointer',
  transition: 'background 0.2s ease-in-out',

  '&:hover': {
    background: '#f5f5f5'
  }
}));
