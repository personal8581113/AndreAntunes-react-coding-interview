export type Group = 'PRIMARY' | 'SECONDARY';

export interface IPerson {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  groups: Group[];
}
