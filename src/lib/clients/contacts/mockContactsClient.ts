import { faker } from '@faker-js/faker';

import { IPerson } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';

function seedContacts(count: number) {
  const res: IPerson[] = [];

  for (let i = 0; i < count; i++) {
    res.push({
      id: faker.datatype.uuid(),
      email: faker.internet.email(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      groups: []
    });
  }

  return res;
}

const delay = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

export class MockContactsClient implements IContactsClient {
  private readonly apiContacts: IPerson[];

  constructor(mockPersonsCount: number) {
    this.apiContacts =
      JSON.parse(localStorage.getItem('contacts')) || seedContacts(mockPersonsCount);
    localStorage.setItem('contacts', JSON.stringify(this.apiContacts));
  }

  async contactList(opts: IContactListArgs): Promise<IContactListResult> {
    const { pageNumber = 1, pageSize = 10 } = opts;

    await delay(1000);
    const skip = (pageNumber - 1) * pageSize;
    const take = pageSize;

    return {
      data: this.apiContacts.slice(skip, skip + take),
      totalCount: this.apiContacts.length
    };
  }

  async getContactById(id: string): Promise<IPerson> {
    await delay(1000);
    try {
      const contact = this.apiContacts.find(c => c.id === id);

      return contact;
    } catch {
      throw new Error(`Error getting contact by id ${id}.`);
    }
  }
  async updateContact(id: string, update: IPerson): Promise<IPerson> {
    await delay(1000);
    const contactIndex = this.apiContacts.indexOf(
      this.apiContacts.find(c => c.id === id)
    );

    this.apiContacts[contactIndex] = update;
    localStorage.setItem('contacts', JSON.stringify(this.apiContacts));
    return update;
  }
}
