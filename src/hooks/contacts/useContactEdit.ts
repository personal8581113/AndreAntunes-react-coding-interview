import { useState, useEffect } from 'react';
import { contactsClient } from '@lib/clients/contacts';
import { IPerson } from '@lib/models/person';
import { useCallback } from 'react';

export function useContactEdit(id: string) {
  console.log(`Getting contact by id ${id}`);

  const [contact, setContact] = useState<IPerson>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchContact = async () => {
      setLoading(true);
      try {
        const contact = await contactsClient.getContactById(id);
        setContact(contact);
      } catch (e) {
        console.error(e);
      } finally {
        setLoading(false);
      }
    };
    fetchContact();
  }, [id]);

  const update = useCallback(async (updated: IPerson) => {
    setLoading(true);
    try {
      await contactsClient.updateContact(id, updated);
      console.log(updated);
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  }, []);

  return {
    contact,
    update,
    loading
  };
}
